
#!/bin/bash
PROJECT=$1
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
TEMP=${WORKING_DIR}/TEMP
SCRIPTS=${WORKING_DIR}/SCRIPTS

cp -n ${REPO}/${PROJECT}/apps-version.env ${REPO}/${PROJECT}/apps-version-template.env

input="${REPO}/${PROJECT}/apps-version-template.env"

while IFS= read -r line
do
value=${line#*=}
if [[ $value != "#!/bin/bash" ]] && [[ ! -z "$value" ]] && [[ $value != *"RELEASE"* ]]; then
  new_line="${line%=*}=\"#VERSION#"\"
  echo "$new_line"
  sed -i s/$line/$new_line/ ${input}
  fi
done < "${input}"
