#!/bin/bash

# Need to provide the project name in $1

PROJECT="$1"
WORKIND_DIR="${HOME}/DEPLOY"
REPO="${WORKIND_DIR}/REPO"
TEMP="${WORKIND_DIR}/TEMP"
PROP="${WORKIND_DIR}/PROP"

echo "${WORKIND_DIR}"
cd "$REPO"
#git clone https://github.com/tariknaeem87/${PROJECT}.git
mkdir -p "$REPO/$PROJECT"
cd "$REPO/$PROJECT"
git checkout -f 1.1
git checkout -b 1.0

if [ "$?" != "0" ]
then 
	echo "Git branch 1.0 is already exist, please check" | exit 0
else 
	echo "Git branch 1.0 doesn't exist hence created branch 1.0"
rm -rf .gitignore

echo "copied all the properties files"
cp ${PROP}/.gitignore ${REPO}/${PROJECT}
cp ${PROP}/assembly.xml ${REPO}/${PROJECT}
cp ${PROP}/pom.xml ${REPO}/${PROJECT}
echo "renamed ****.sh to microservice.sh"
[ ! -f ${REPO}/${PROJECT}/microservice.sh ] && mv *.sh microservice.sh 

fi
