#!/bin/bash
##################################################################################################
###PGM-NAME: Deploy.sh                                                                       #
###Version : V1.0(Initial)                                                                       #
###Purpose : Automates Manuall config changes for TIM apps		                                 #
###Usage   : Deploy.sh PROJECT NAME URI_VARIABLE ENV                                             #
###Date of Creation : 07-July-2019                                                                #
##################################################################################################

if [ $# -gt 0 ] && [ $# -eq 3 ] ;then

PROJECT=$1
URI_VAR=$2
ENV=$3
else
echo "Oops !! Please provide Project Name ,URI Varible and ENV  details"
exit 1
fi
echo "Automating Migration Process for project $PROJECT "
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
TEMP=${WEORKING_DIR}/TEMP
SCRIPTS=${WORKING_DIR}/SCRIPTS

cd ${REPO}/${PROJECT}
#check current branch
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
echo "working on current branch ${branch}"

echo " Staring auto-ms.sh scrip execution "
sh -x ${SCRIPTS}/auto-ms.sh ${PROJECT} 
echo " auto-ms.sh completed"
echo "Staring microservice.sh editing script"
sh -x ${SCRIPTS}/ms_sh_edit.sh ${PROJECT}
echo "completed microservice.sh editing script"
echo "Starting microservice.env file editing script"
sh -x ${SCRIPTS}/ms_env_edit.sh ${PROJECT}
echo "completed editing of microservice.env & microservice-prod.env files"
echo "Starting apps_version editing script"
sh -x ${SCRIPTS}/apps_version_template.sh ${PROJECT}
echo "Completed editing apps_version file"
echo "Starting pom file editing"
sh -x ${SCRIPTS}/pom.sh ${PROJECT}
echo "Completed adding and editing pom file"


