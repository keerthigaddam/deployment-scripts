#-z]' '[A-Z]' < ${TEMP}/ms_env_apps2.txt  > ${TEMP}/ms_env_appsb.txt ; mv ${TEMP}/ms_env_appsb.txt ${TEMP}/ms_env_apps2.txt!/bin/bash
##################################################################################################
###PGM-NAME: ms_sh_edit.sh                                                                           #
###Version : V1.0(Initial)                                                                       #
###Purpose : Automates Manuall config changes for microservice.sh                                #
###Date of Creation : 07-July-3019                                                               #
##################################################################################################
PROJECT=$1
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
TEMP=${WORKING_DIR}/TEMP
SCRIPTS=${WORKING_DIR}/SCRIPTS
f_name=${REPO}/${PROJECT}/microservice.env
mkdir -p  ${TEMP}/PROD 2>/dev/null
cd $REPO
cat $f_name > ${TEMP}/ms_env_main_temp.txt
sed -i '/source/d'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
src=$(cat $PROP/source.txt)
sed -i "2i\
$src"  ${TEMP}/ms_env_main_temp.txt
sed -i '/if/,/fi/d' ${TEMP}/ms_env_main_temp.txt 
#sed -i '$ d' ${TEMP}/ms_env_main_temp.txt
old=$(cat $PROP/old.txt)
new=$(cat $PROP/new.txt)
sed -i 's,'"${old}"','"${new}"',' "${TEMP}/ms_env_main_temp.txt"
sed -i '/^$/d' ${TEMP}/ms_env_main_temp.txt
sed -n '/echo/,/"/p' ${f_name} > ${TEMP}/ms_env_temp.txt
sed -i '1d;$d' ${TEMP}/ms_env_temp.txt
u=$(cat $PROP/uri.txt)
url=$(cat $PROP/url.txt)
echo $u
sed -i  's,'"${u}"','"${url}"',' "${TEMP}/ms_env_temp.txt" 
awk -F "=" '{print $1}' ${TEMP}/ms_env_temp.txt  > ${TEMP}/ms_env_apps1.txt
sed -i 's/\#//' ${TEMP}/ms_env_apps1.txt
for i in ` cat ${TEMP}/ms_env_apps1.txt `
do
echo $i > ${TEMP}/1.txt

tr '[a-z]' '[A-Z]' <  ${TEMP}/1.txt > ${TEMP}/ms_env_apps2.txt 
sed -i 's/\./\_/g' ${TEMP}/ms_env_apps2.txt 
sed -i 's/\-/\_/g' ${TEMP}/ms_env_apps2.txt;sed -i 's/$/\_VERSION\}/g' ${TEMP}/ms_env_apps2.txt;sed -i 's/ //g' ${TEMP}/ms_env_apps2.txt
sed -i 's/^/\$\{/g' ${TEMP}/ms_env_apps2.txt
sed -i 's/^.//' ${TEMP}/ms_env_apps2.txt

k=$(cat ${TEMP}/ms_env_apps2.txt)
echo $k
grep  $i ${TEMP}/ms_env_temp.txt > ${TEMP}/inter.txt
sed  -i 's/{.*//'  ${TEMP}/inter.txt 
sed -i "$ s/$/$k/" ${TEMP}/inter.txt 
cat ${TEMP}/inter.txt >> ${TEMP}/OUT.txt
done
sed -i '1i echo \"\\ ' ${TEMP}/OUT.txt
echo "\"\\" >> ${TEMP}/OUT.txt
grep ".df" ${TEMP}/ms_env_main_temp.txt >> ${TEMP}/OUT.txt
sed -n '/echo/q;p' ${TEMP}/ms_env_main_temp.txt > ${TEMP}/OUT1.txt
n=5
tac ${TEMP}/OUT1.txt | sed "1,$n{d}" | tac > ${TEMP}/OUT2.txt
sed -i '/^DEFAULT/d' ${TEMP}/OUT2.txt
sed -i '/^COMMON/d' ${TEMP}/OUT2.txt
cat ${PROP}/common.txt >>  ${TEMP}/OUT2.txt

echo " " >> ${TEMP}/OUT2.txt
cat ${TEMP}/OUT.txt >>  ${TEMP}/OUT2.txt
mv ${TEMP}/OUT2.txt ${TEMP}/microservice_temp.env
#script to create prod env file
sh -x ${SCRIPTS}/ms_env_prod_edit.sh $PROJECT
cp ${TEMP}/PROD/OUT2.txt ${TEMP}/microservice-prod_temp.env

#cleanup temp dir leaving env files
rm -rf ${TEMP}/*.txt 
rm -rf ${TEMP}/PROD/*.txt

sed -i  '1,$d' ${REPO}/${PROJECT}/microservice.env
cat ${TEMP}/microservice_temp.env >> ${REPO}/${PROJECT}/microservice.env
sed -i '1,$d' ${REPO}/${PROJECT}/microservice-prod.env
cat ${TEMP}/microservice-prod_temp.env >> ${REPO}/${PROJECT}/microservice-prod.env


