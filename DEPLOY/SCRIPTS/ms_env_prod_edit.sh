#-z]' '[A-Z]' < ${TEMP}/ms_env_apps2.txt  > ${TEMP}/ms_env_appsb.txt ; mv ${TEMP}/ms_env_appsb.txt ${TEMP}/ms_env_apps2.txt!/bin/bash
##################################################################################################
###PGM-NAME: ms_sh_edit.sh                                                                           #
###Version : V1.0(Initial)                                                                       #
###Purpose : Automates Manuall config changes for microservice.sh                                #
###Date of Creation : 07-July-3019                                                               #
##################################################################################################
PROJECT=$1
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
mkdir -p ${WORKING_DIR}/TEMP/PROD
TEMP=${WORKING_DIR}/TEMP/PROD
SCRIPTS=${WORKING_DIR}/SCRIPTS
f_name=${REPO}/${PROJECT}/microservice-prod.env
cd $REPO
cat $f_name > ${TEMP}/ms_env_main_temp.txt
sed -i '/source/d'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
sed -i '/LEGACY/{x;p;x;}'  ${TEMP}/ms_env_main_temp.txt
src=$(cat $PROP/source.txt)
sed -i "2i\
$src"  ${TEMP}/ms_env_main_temp.txt
sed -i '/if/,/fi/d' ${TEMP}/ms_env_main_temp.txt 
#sed -i '$ d' ${TEMP}/ms_env_main_temp.txt
sed -n '/echo /q;p' ${TEMP}/ms_env_main_temp.txt > ${TEMP}/OUT1.txt
sed -i '/^$/d' ${TEMP}/OUT1.txt
n=5
tac ${TEMP}/OUT1.txt | sed "1,$n{d}" | tac > ${TEMP}/OUT2.txt
sed -i '/^DEFAULT/d' ${TEMP}/OUT2.txt
sed -i '/^COMMON/d' ${TEMP}/OUT2.txt
cat ${PROP}/common.txt >>  ${TEMP}/OUT2.txt
cat ${WORKING_DIR}/TEMP/OUT.txt >>  ${TEMP}/OUT2.txt

