#!/bin/bash
##################################################################################################
###PGM-NAME: ms_sh_edit.sh                                                                           #
###Version : V1.0(Initial)                                                                       #
###Purpose : Automates Manuall config changes for microservice.sh                                #
###Date of Creation : 07-July-2019                                                               #
##################################################################################################
PROJECT=$1
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
TEMP=${WORKING_DIR}/TEMP
SCRIPTS=${WORKING_DIR}/SCRIPTS
manifest=${REPO}/${PROJECT}/manifest
file=${REPO}/${PROJECT}/microservice.sh
i=$(cat ${PROP}/stream.txt)
p_id=$(echo "$PROJECT" | awk -F "-" '{print $1}')
echo $id
if [[ $p_id =~ [0-9] ]];then
      echo "Input contains number"
 pro_id=$p_id
echo $pro_id  
 else
      echo "Input contains non numerical value"
 pro_id=$(echo "$PROJECT" | awk -F "-" '{print $2}')  
echo $pro_id
fi
cd ${REPO}/${PROJECT}

#check current branch
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
echo "working on current branch ${branch}"
echo "editing microservice.sh for project ${PROJECT} on branch ${branch}"

sed -e '/^#/d' -e 's/#.*$//' ${file} > ${TEMP}/ms_sh_temp_1.txt
if [ $(grep -c  MS_ID  ${file}) -ne 0 ] 
then 

sed -n '/MS_ID/,$p' ${TEMP}/ms_sh_temp_1.txt  > ${TEMP}/ms_sh_temp.txt
sed -i '1i #!/bin/bash ' ${TEMP}/ms_sh_temp.txt
sed -i '/if/,/fi/d' ${TEMP}/ms_sh_temp.txt 
sed -i  '/MS_ID=/{G;G;G;G;G;G;G}' ${TEMP}/ms_sh_temp.txt
else
sed -e '/^#/d' -e 's/#.*$//' ${file} > ${TEMP}/ms_sh_temp.txt
sed -i '1i #!/bin/bash ' ${TEMP}/ms_sh_temp.txt
sed -i '/if/,/fi/d' ${TEMP}/ms_sh_temp.txt
msid=(MS_ID=\""$pro_id\"")
sed -i "2i\
$msid" ${TEMP}/ms_sh_temp.txt
fi
 
#check if monolithic apps are present
if [ -d "${manifest}" ] 
then
cd ${manifest}
a=$(ls -ltr ${manifest}/*.yml | wc -l )
b=$(ls -ltr ${manifest}/*.yaml | wc -l )
mc=$(( $a + $b ))
echo "No:of monolithic apps are : $mc "
MONO=(MONOS=\""$((mc))\"")
echo $MONO
sed -i '3i # Number of mono apps\' ${TEMP}/ms_sh_temp.txt
sed -i "4i\
$MONO" ${TEMP}/ms_sh_temp.txt 
sed -i  '/MONOS=/{G;G;G;G;G;G;G}' ${TEMP}/ms_sh_temp.txt

#for (( j=1; j<= $mc; j++ ))
#do
#echo $j
#m_p="M"$j"_HOSTNAME=\${M"$j"_MONO_HOST_NAME}\${APP_ENV_HOSTNAME}"
#echo $m_p
#if [ $(grep -c ${m_p} ${file}) -ne 0 ]
#then
#echo " already monos host name string is present check the file"
#$else
#sed -i "5i\
#$m_p" ${TEMP}/ms_sh_temp.txt
#fi
#done

else
mc=0
echo "No monolithi apps found for the project ${PROJECT}"
MONO=(MONOS=\""$((mc))\"")
echo $MONO
sed -i '3i # Number of mono apps\' ${TEMP}/ms_sh_temp.txt
sed -i "4i\
$MONO" ${TEMP}/ms_sh_temp.txt
fi

#check the no of stream
s=$(grep -w "$i" ${file} | wc -l)
echo "no:of streams for project ${PROJECT} : $s "
st=(STREAMS=\""$((s))\"")
echo $st
sed -i '8i #Number of streams\' ${TEMP}/ms_sh_temp.txt
sed -i "9i\
$st" ${TEMP}/ms_sh_temp.txt
sed -i  '/STREAMS=/{G;G;G;G;G}' ${TEMP}/ms_sh_temp.txt
l=$(grep -n echo ${TEMP}/ms_sh_temp.txt | sed -n '2p' | awk -F ":" '{print $1}')

echo $l
head -n $l ${TEMP}/ms_sh_temp.txt > ${TEMP}/2.txt ; mv ${TEMP}/2.txt ${TEMP}/ms_sh_temp.txt
msid=(MS_ID=\""$pro_id\"")
echo $msid
echo "$msid" >> ${TEMP}/ms_sh_temp.txt
tac ${TEMP}/ms_sh_temp.txt | sed "2,3d" | tac > ${TEMP}/2.txt ; mv ${TEMP}/2.txt ${TEMP}/ms_sh_temp.txt
nfs_count=$(grep FULL ${TEMP}/ms_sh_temp.txt | grep \ | awk -F "{" '{print $2}' | awk -F "_" '{print $1}')
echo $nfs_count | tr ' ' '\n' > ${TEMP}/nfs.txt
for k in ` cat ${TEMP}/nfs.txt `
do
mch="${k}_DEF"
echo ${mch}
nfsstr=""${k}"_BIND_NFS=\"\${CF_SERVICE_NAME}\""
echo ${nfsstr}
sed -i "/${mch}/a ${nfsstr} " ${TEMP}/ms_sh_temp.txt
done
sed -i '/^echo/d' ${TEMP}/ms_sh_temp.txt | sed -i '/^>/d' ${TEMP}/ms_sh_temp.txt | sed -i '/^\$/d'  ${TEMP}/ms_sh_temp.txt 
sed -i '/^$/d' ${TEMP}/ms_sh_temp.txt
file_nfs=${TEMP}/nfs.txt
if [ $s -eq 0  ]
then 
echo "STreams are not included"
sed -i  '/^OPTION/d' ${TEMP}/ms_sh_temp.txt
sed -i '/^FULL_DIR/d' ${TEMP}/ms_sh_temp.txt
else
echo "Streams are included"
fi

sed -i '1,$d' ${REPO}/${PROJECT}/microservice.sh
cat ${TEMP}/ms_sh_temp.txt >> ${REPO}/${PROJECT}/microservice.sh

rm -rf  ${TEMP}/*.txt
