

#!/bin/bash   
PROJECT=$1
WORKING_DIR=$HOME/DEPLOY
REPO=${WORKING_DIR}/REPO
PROP=${WORKING_DIR}/PROP
TEMP=${WORKING_DIR}/TEMP
SCRIPTS=${WORKING_DIR}/SCRIPTS
file_name=${REPO}/microservice.env
cp ${PROP}/pom.xml ${REPO}/pom.xml
sed -i "s/{PROJECTNAME}/${PROJECT}/g" "${PROP}/pom.xml"

sed -n '/echo/,/"/p' ${file_name} > ${TEMP}/pom.tmp 
sed -i '1d;$d' ${TEMP}/pom.tmp

while IFS= read -r line
do
value=${line#*//}
if [[ $value != *"springframework"* ]]; then
GROUPID=$( echo $value | awk -F ":" '{print $1}' ) 
ARTIFACTID=$( echo $value | awk -F ":" '{print $2}' ) 
VERSION=$( echo $value | awk -F ":" '{print $3}'  | tr -d '${' | tr -d '}' ) 
VERSION=$(grep $VERSION ${REPO}/apps-version.env | sed 's/^.*=//g' | sed 's/"//g' )
sed -i "54i  \   \     <dependency>\n            <groupId>${GROUPID}</groupId>\n            <artifactId>${ARTIFACTID}</artifactId>\n            <version>${VERSION}</version>\n        </dependency>" ${REPO}/pom.xml
fi
done < "${TEMP}/pom.tmp"


